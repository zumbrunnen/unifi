FROM ubuntu:focal
LABEL maintainer="David Zumbrunnen <zumbrunnen@gmail.com>"

ENV DEBIAN_FRONTEND=noninteractive
ENV UNIFI_VERSION=8.3.32

RUN apt-get update && apt-get -y install binutils libcap2 curl openjdk-17-jre-headless mongodb-server \
    jsvc gnupg ca-certificates logrotate
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN curl -o unifi_sysvinit_all.deb http://dl.ubnt.com/unifi/${UNIFI_VERSION}/unifi_sysvinit_all.deb && \
    dpkg -i unifi_sysvinit_all.deb && \
    rm unifi_sysvinit_all.deb

VOLUME ["/var/lib/unifi"]

EXPOSE 8080 8081 8443 8843 8880 3478

WORKDIR /var/lib/unifi

ENTRYPOINT ["/usr/bin/java", "-Xmx1024M", "-jar", "/usr/lib/unifi/lib/ace.jar"]

CMD ["start"]
